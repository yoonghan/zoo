"use client";

import React, { memo, useCallback, useEffect, useRef, useState } from "react";
import style from "./mini-menu.module.css";

export type MiniMenuItems = {
  hashId: string;
  title: string;
};

type MiniMenuProps = {
  model: MiniMenuItems[];
  onScrollMonitor?: () => void;
};

function MiniMenu({ model, onScrollMonitor }: MiniMenuProps) {
  const navBar = useRef<HTMLDivElement>(null);
  /* c8 ignore next */
  const [navBarPosition, setNavBarPosition] = useState(0);

  const addStickyToScroll = useCallback(() => {
    if (onScrollMonitor) {
      onScrollMonitor(); // use to monitor unmount
    }
    if (navBar.current) {
      if (window.scrollY >= navBarPosition) {
        navBar.current.classList.add(style.sticky);
      } else {
        navBar.current.classList.remove(style.sticky);
      }
    }
  }, [navBarPosition, onScrollMonitor]);

  useEffect(() => {
    setNavBarPosition(navBar.current?.offsetHeight || 0);
    window.addEventListener("scroll", addStickyToScroll);
    return () => window.removeEventListener("scroll", addStickyToScroll);
  }, [addStickyToScroll]);

  return (
    <nav className="overflow-x-auto shadow-md" ref={navBar}>
      <div
        className={`${style.container} flex gap-4 justify-center p-6 min-w-max`}
      >
        {model.map((item, idx) => (
          <React.Fragment key={item.hashId}>
            {idx !== 0 && (
              <div
                className={`${style.separator} border-r border-1`}
                role="separator"
              ></div>
            )}
            <a href={`#${item.hashId}`}>{item.title}</a>
          </React.Fragment>
        ))}
      </div>
    </nav>
  );
}

export default memo(MiniMenu, () => true);
